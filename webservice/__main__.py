from gidgetlab.aiohttp import GitLabBot

import os
import aiohttp

from aiohttp import web

from gidgetlab import routing, sansio
from gidgetlab import aiohttp as gl_aiohttp

router = routing.Router()

@router.register("Issue Hook", action="open")
async def issue_opened_event(event, gl, *args, **kwargs):
    """Whenever an issue is opened, greet the author and say thanks.

    Args:
        event ([type]): [description]
        gl ([type]): [description]
    """
    url = f"/projects/{event.project_id}/issues/{event.object_attributes['iid']}/notes"
    message = f"gracias for the report @{event.data['user']['username']}! On it.. STAT (danger will robinson👘)."
    await gl.post(url, data={"body": message})

async def main(request):
    # read the GitLab webhook payload
    body = await request.read()

    # our authentication token and secret
    secret = os.environ.get("GL_SECRET")
    access_token = os.environ.get("GL_ACCESS_TOKEN")

    # a representation of GitLab webhook event
    event = sansio.Event.from_http(request.headers, body, secret=secret)

    async with aiohttp.ClientSession() as session:
        gl = gl_aiohttp.GitLabAPI(session, "sskenner", access_token=access_token)

        # call the appropriate callback for the event
        await router.dispatch(event, gl)
    
    # return a "Success"
    return web.Response(status=200)

if __name__ == "__main__":

    app = web.Application()
    app.router.add_post("/", main)
    port = os.environ.get("PORT")
    if port is not None:
        port = int(port)
    
    web.run_app(app, port=port)
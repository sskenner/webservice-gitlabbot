from gidgetlab.aiohttp import GitLabBot

bot = GitLabBot("sskenner")

@bot.router.register("Issue Hook", action="open")
async def issue_opened_event(event, gl, *args, **kwargs):
    """Whenever an issue is opened, greet the author and say thanks.

    Args:
        event ([type]): [description]
        gl ([type]): [description]
    """
    url = f"/projects/{event.project_id}/issues/{event.object_attributes['iid']}/notes"
    message = f"gracias for the report @{event.data['user']['username']}! On it.. STAT (danger will robinson👘)."
    await gl.post(url, data={"body": message})

if __name__ == "__main__":
    bot.run()